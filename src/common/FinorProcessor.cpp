#include "gt/swatch/FinorProcessor.hpp"


#include "swatch/core/Factory.hpp"

#include "gt/swatch/FinorAlgo.hpp"


SWATCH_REGISTER_CLASS(gt::swatch::FinorProcessor)


namespace gt {
namespace swatch {

FinorProcessor::FinorProcessor(const ::swatch::core::AbstractStub& aStub) :
  DaughterCard(aStub, new FinorAlgo([this] () -> emp::Controller& { return this->getController(); }))
{
  // If need to write to any control bus registers in GT algorithm before it processes events,
  // register commands (and add to standard FSMs) here in future
}


FinorProcessor::~FinorProcessor()
{
}

} // namespace swatch
} // namespace gt
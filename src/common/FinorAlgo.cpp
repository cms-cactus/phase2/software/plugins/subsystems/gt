#include "gt/swatch/FinorAlgo.hpp"


#include "swatch/core/Factory.hpp"

#include "gt/swatch/FinorProcessor.hpp"


namespace gt {
namespace swatch {

FinorAlgo::FinorAlgo(const std::function<emp::Controller&()>& f) :
  getController(f)
  // REGISTER MONITORING DATA AS METRICS HERE
{
}


FinorAlgo::~FinorAlgo()
{
}


void FinorAlgo::retrieveMetricValues()
{
  emp::Controller& controller = getController();
  uhal::HwInterface& hw = getController().hw();
  // UPDATE VALUES OF METRICS HERE
}

} // namespace swatch
} // namespace gt
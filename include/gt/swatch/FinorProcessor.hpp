
#ifndef __GT_SWATCH_FINORPROCESSOR_HPP__
#define __GT_SWATCH_FINORPROCESSOR_HPP__


#include "serenity/swatch/DaughterCard.hpp"


namespace gt {
namespace swatch {

class FinorProcessor : public serenity::swatch::DaughterCard {
public:
  FinorProcessor(const ::swatch::core::AbstractStub&);
  ~FinorProcessor();
};

} // namespace swatch
} // namespace gt


#endif

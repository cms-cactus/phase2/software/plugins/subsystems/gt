
#ifndef __GT_SWATCH_FINORALGO_HPP__
#define __GT_SWATCH_FINORALGO_HPP__


#include "swatch/phase2/AlgoInterface.hpp"


namespace emp {
class Controller;
}

namespace gt {
namespace swatch {

class FinorAlgo : public ::swatch::phase2::AlgoInterface {
public:
  FinorAlgo(const std::function<emp::Controller&()>&);
  ~FinorAlgo();

private:
  void retrieveMetricValues() final;

  std::function<emp::Controller&()> getController;
};

} // namespace swatch
} // namespace gt


#endif
